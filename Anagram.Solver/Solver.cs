﻿using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Anagram.Solver
{
    public class AnagramSolver
    {
        private readonly string _anagram;
        private readonly string _sortedAnagram;
        private readonly Dictionary<char, int> _anagramMap;
        private readonly string _hash;
        private readonly List<string> _wordlist;

        public AnagramSolver(string anagram, string hash, List<string> wordlist)
        {
            if (string.IsNullOrWhiteSpace(anagram))
                throw new ArgumentException("anagram");

            if (string.IsNullOrWhiteSpace(hash))
                throw new ArgumentException("hash");

            if (wordlist == null || wordlist.Count == 0)
                throw new ArgumentException("wordlist");

            _anagram = anagram;
            _sortedAnagram = _anagram.Sort();
            _anagramMap = _anagram.ToMap();
            _hash = hash;
            _wordlist = wordlist;
        }

        public string Solve()
        {
            var sequence = _wordlist.ToList();
            var maxWords = 3;

            while (true)
            {
                if (maxWords < 1)
                    return string.Empty;

                var result = sequence.FirstOrDefault(
                    w => w.Length == _anagram.Length &&
                    w.Sort() == _sortedAnagram &&
                    w.Hash() == _hash);

                if (result != null)
                    return result;

                var queries = new ConcurrentQueue<IEnumerable<string>>();
                foreach (var word in sequence)
                {
                    var q = _wordlist.Select(word2 => $"{word} {word2}")
                        .Where(w => w.ToMap().IsSubset(_anagramMap));

                    queries.Enqueue(q);
                }

                var sw = Stopwatch.StartNew();
                var dictionary = new ConcurrentQueue<string>();

                var tasks = queries.Select(q => ExecuteQuery(q, dictionary)).ToArray();

                foreach (var task in tasks)
                {
                    task.Wait();
                    if (task.Result != null)
                    {
                        return task.Result;
                    }
                }
                sw.Stop();

                sequence = dictionary.ToList();
                maxWords = maxWords - 1;

                Console.WriteLine($"sequence: {sequence.Count} wordlist: {_wordlist.Count} maxWords: {maxWords} elapsed: {sw.ElapsedMilliseconds}ms");
            }
        }

        private Task<string> ExecuteQuery(IEnumerable<string> query, ConcurrentQueue<string> result)
        {
            return Task.Factory.StartNew(() =>
            {
                foreach (var word in query)
                {
                    result.Enqueue(word);
                    if (result.Count % 250000 == 0)
                    {
                        var r = result.FirstOrDefault(
                            w => w.Length == _anagram.Length &&
                                 w.Sort() == _sortedAnagram &&
                                 w.Hash() == _hash);
                        if (r != null)
                        {
                            return r;
                        }
                        Console.WriteLine($"{result.Count}");
                    }
                }

                return null;
            });
        }
    }
}
