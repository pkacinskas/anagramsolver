﻿using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace Anagram.Solver
{
    public static class Utils
    {
        public static List<string> LoadWordlist(string path)
        {
            if (!File.Exists(path))
                throw new FileNotFoundException("wordlist file missing");

            return File.ReadAllLines(path).Select(l => l.ToLower()).ToList();
        }

        public static List<string> FilterWordlist(List<string> words, string anagram)
        {
            return words
                .Where(w => w.ToCharArray().Intersect(anagram.ToCharArray()).Any())
                .Where(w => w.ToMap().IsSubset(anagram.ToMap()))
                .OrderByDescending(w => w.Length)
                .ToList();
                
        }

        public static List<string> RemoveDuplicates(List<string> input)
        {
            return input
                .GroupBy(word => word)
                .ToDictionary(grp => grp.Key, grp => grp.Count())
                .Select(x => x.Key)
                .ToList();
        }

        public static Dictionary<char, int> ToMap(this string word)
        {
            return word
                .Replace(" ", "")
                .GroupBy(c => c)
                .ToDictionary(grp => grp.Key, grp => grp.Count());
        }

        public static bool IsSubset(this Dictionary<char, int> set, Dictionary<char, int> of)
        {
            return !set.Any(c => c.Value > of.GetValue(c.Key, 0));
        }

        private static TV GetValue<TK, TV>(this IDictionary<TK, TV> dict, TK key, TV def = default(TV))
        {
            TV val;
            return dict.TryGetValue(key, out val) ? val : def;
        }

        public static string Hash(this string input)
        {
            var md5 = MD5.Create();
            var bytes = Encoding.ASCII.GetBytes(input);
            var hash = md5.ComputeHash(bytes);
            var sb = new StringBuilder();

            foreach (var h in hash)
                sb.Append(h.ToString("x2"));

            return sb.ToString();
        }

        public static string Sort(this string input)
        {
            return string.Concat(input.Replace(" ", "").OrderBy(c => c));
        }
    }
}
