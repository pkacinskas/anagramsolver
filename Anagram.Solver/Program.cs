﻿using System;
using System.Configuration;
using System.Diagnostics;

namespace Anagram.Solver
{
    class Program
    {
        static void Main()
        {
            var anagram = ConfigurationManager.AppSettings["anagram"];
            var hash = ConfigurationManager.AppSettings["hash"];
            var path = ConfigurationManager.AppSettings["wordlist"];

            var wordlist = Utils.FilterWordlist(
                Utils.RemoveDuplicates(
                    Utils.LoadWordlist(path)),
                anagram);

            var solver = new AnagramSolver(anagram, hash, wordlist);

            var sw = Stopwatch.StartNew();
            var result = solver.Solve();
            sw.Stop();

            Console.WriteLine($"result: {result}\r\ntotal elapsed: {sw.ElapsedMilliseconds}ms");
            Console.ReadKey();
        }        
    }
}
