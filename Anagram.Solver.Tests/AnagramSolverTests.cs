﻿using NUnit.Framework;
using System;
using FluentAssertions;

namespace Anagram.Solver.Tests
{
    [TestFixture]
    public class AnagramSolverTests
    {
        [Test]
        public void SolveAnagramTest()
        {
            const string anagram = "god balm";
            const string answer = "lamb dog";

            var hash = answer.Hash();

            var p = AppDomain.CurrentDomain.SetupInformation.ApplicationBase + "\\wordlist";

            var wordlist = Utils.FilterWordlist(Utils.RemoveDuplicates(Utils.LoadWordlist(p)), anagram);

            var solver = new AnagramSolver(anagram, hash, wordlist);

            var result = solver.Solve();

            result.Should().Be(answer);
        }

        [Test]
        public void TestMap()
        {
            var input = "test to map";

            var result = input.ToMap();

            result['t'].Should().Be(3);
            var value = 0;
            result.TryGetValue(' ', out value).Should().BeFalse();
        }

        [Test]
        public void NotSubsetOfSh()
        {
            var test = "poultry outwits ants";
            var test1 = "poultry outwits ant's";

            var result = test1.ToMap().IsSubset(test.ToMap());

            result.Should().BeFalse();
        }

        [Test]
        public void SubsetOfSmallerWord()
        {
            var test = "poultry outwits ants";
            var test1 = "apluwtors";

            var result = test1.ToMap().IsSubset(test.ToMap());

            result.Should().BeTrue();
        }

        [Test]
        public void NotSubsetOfSmallerWord()
        {
            var test = "poultry outwits ants";
            var test1 = "abductors";

            var result = test1.ToMap().IsSubset(test.ToMap());

            result.Should().BeFalse();
        }
    }
}
